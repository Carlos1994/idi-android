package com.example.pr_idi.mydatabaseexample;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.Comparator;
import java.util.List;


/**
 * Created by eric on 25/12/16.
 */

public class RecyclerViewClass extends AppCompatActivity {

    public static RecyclerView mRecyclerView;
    public static RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    public static Boolean isOneToTwo = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recycler);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.recyclerFab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), Alta.class));
            }
        });

        if (getIntent().hasExtra("isOneToTwo")) {
            isOneToTwo = getIntent().getExtras().getBoolean("isOneToTwo");
        }

        MainDrawer.filmData.open();

        List<Film> values = MainDrawer.filmData.getAllFilms();

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);

        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new MyAdapter(values, isOneToTwo, this);
        mRecyclerView.setAdapter(mAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.recycler_view, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            Toast toast = Toast.makeText(getApplicationContext(), "Settings", Toast.LENGTH_LONG);
            toast.show();
            return true;
        }
        else if (id == R.id.options_menu_main_search) {
            Toast toast = Toast.makeText(getApplicationContext(), "Search", Toast.LENGTH_LONG);
            toast.show();
            return true;
        }
        /*else if (id == R.id.sort_atoz) {
            Intent intent = new Intent (this, MainDrawer.class);
            intent.putExtra("isAtoZ", true);
            startActivity(intent);
        }
        else if (id == R.id.sort_ztoa) {
            Intent intent = new Intent (this, MainDrawer.class);
            intent.putExtra("isAtoZ", false);
            startActivity(intent);
        }
        else */if (id == R.id.sort_onetotwo) {
            List<Film> values = MainDrawer.filmData.getAllFilms();
            mAdapter = new MyAdapter(values, true, this);
            mRecyclerView.setAdapter(mAdapter);
        }
        else if (id == R.id.sort_twotoone) {
            List<Film> values = MainDrawer.filmData.getAllFilms();
            mAdapter = new MyAdapter(values, false, this);
            mRecyclerView.setAdapter(mAdapter);
        }

        return super.onOptionsItemSelected(item);
    }
}
