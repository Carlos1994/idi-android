package com.example.pr_idi.mydatabaseexample;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

/**
 * Created by eric on 25/12/16.
 */

public class Alta extends AppCompatActivity{

    private String titleToChange = "";
    private String countryToChange = "";
    private int releaseToChange = 0;
    private String directorToChange = "";
    private String protagonistToChange = "";
    private int criticsToChange = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alta);

        MainDrawer.filmData.open();

        EditText title = (EditText)findViewById(R.id.editTextTitle);
        EditText country = (EditText)findViewById(R.id.editTextCountry);
        final EditText release = (EditText)findViewById(R.id.editTextRelease);
        EditText director = (EditText)findViewById(R.id.editTextDirector);
        EditText protagonist = (EditText)findViewById(R.id.editTextProtagonist);
        //EditText critics = (EditText)findViewById(R.id.editTextCritics);
        RatingBar critics = (RatingBar) findViewById(R.id.criticsBar);

        title.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                titleToChange = editable.toString();
            }
        });

        country.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                countryToChange = editable.toString();
            }
        });

        release.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().isEmpty()) releaseToChange = 0;
                else releaseToChange = Integer.parseInt(editable.toString());
            }
        });

        director.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                directorToChange = editable.toString();
            }
        });

        protagonist.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                protagonistToChange = editable.toString();
            }
        });

        /*critics.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().isEmpty()) criticsToChange = 0;
                else criticsToChange = Integer.parseInt(editable.toString());
            }
        });*/

        critics.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                if (b) {
                    criticsToChange = (int)v*2;
                }
            }
        });

        Button updateButton = (Button) findViewById(R.id.updateButton);
        updateButton.setOnClickListener(new View.OnClickListener() {
            public void onClick (View v) {
                if (titleToChange.isEmpty() || countryToChange.isEmpty() || (releaseToChange == 0) || directorToChange.isEmpty() || protagonistToChange.isEmpty()) {
                    final Snackbar snackbar = Snackbar.make(v, "One or more fields are empty!", Snackbar.LENGTH_INDEFINITE);
                    snackbar.setAction("OK", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    snackbar.dismiss();
                                }
                            });
                    View viewS = snackbar.getView();
                    TextView tv = (TextView) viewS.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextSize(18);
                    snackbar.show();
                }
                else {
                    if (criticsToChange == 0) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(Alta.this);
                        builder.setMessage("Critics rate field is set to zero, do you want to proceed?");
                        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, final int j) {
                                MainDrawer.filmData.createFilm(titleToChange, countryToChange, releaseToChange, directorToChange, protagonistToChange, criticsToChange);
                                Intent intent = new Intent(getApplicationContext(), MainDrawer.class);
                                intent.putExtra("databaseUpdated", true);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                finish();
                            }
                        });
                        builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    } else {
                        MainDrawer.filmData.createFilm(titleToChange, countryToChange, releaseToChange, directorToChange, protagonistToChange, criticsToChange);
                        Intent intent = new Intent(getApplicationContext(), MainDrawer.class);
                        intent.putExtra("databaseUpdated", true);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    }
                }
            }
        });
    }
}
