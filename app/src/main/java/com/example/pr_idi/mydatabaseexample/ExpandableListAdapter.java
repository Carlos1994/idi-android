package com.example.pr_idi.mydatabaseexample;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.text.Spannable;
import android.text.style.BackgroundColorSpan;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

/**
 * Created by Carlos on 02/01/2017.
 */

public class ExpandableListAdapter extends BaseExpandableListAdapter{

    private Activity context;
    private ArrayAdapter<Film> films;
    private WindowManager windowManager;
    private boolean searching;
    private String search;


    public ExpandableListAdapter(Activity context, ArrayAdapter<Film> values, WindowManager wM, boolean search) {
        this.context = context;
        this.films = values;
        this.windowManager = wM;
        this.searching = search;
        this.search = "";
    }

    public void setAdapterValues(ArrayAdapter<Film> values) {
        this.films = values;
    }

    public void setSearching(boolean s) {
        this.searching = s;
    }

    public void setSearch(String s) {
        this.search = s;
    }

    @Override
    public int getGroupCount() {
        return films.getCount();
    }

    @Override
    public int getChildrenCount(int i) {
        return 1;
        //Title, Director, Protagonist, Year, Critics rate and Country
    }

    @Override
    public Object getGroup(int i) {
        return null;
    }

    @Override
    public Object getChild(int i, int i1) {
        return null;
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        Film f = films.getItem(i);
        if (view == null) {
            LayoutInflater infalInflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = infalInflater.inflate(R.layout.group_item, null);
        }

        //return setFilmAttributes(view, f);
        TextView item = (TextView) view.findViewById(R.id.film_title);
        item.setTypeface(null, Typeface.BOLD);
        String title = f.getTitle();
        if (searching && !search.isEmpty() &&
                (title.matches(search.concat("(.*)")) || title.toLowerCase().matches(search.toLowerCase().concat("(.*)")))) {
            item.setText(highlighText(title));
        }
        else item.setText(title);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        item.setWidth(displayMetrics.widthPixels);

        return view;
    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = context.getLayoutInflater();
        Film f = films.getItem(i);
        if (view == null) {
            view = inflater.inflate(R.layout.film_item, null, false);
        }

        return setFilmAttributes(view, f);
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }

    private View setFilmAttributes(final View view, Film f) {
        TextView item;
        //Director
        item = (TextView) view.findViewById(R.id.director);
        item.setText(f.getDirector());

        //Protagonist
        item = (TextView) view.findViewById(R.id.protagonist);
        String prot = f.getProtagonist();
        if (searching && !search.isEmpty() &&
                (prot.matches(search.concat("(.*)")) || prot.toLowerCase().matches(search.toLowerCase().concat("(.*)")))) {
            item.setText(highlighText(prot));
        }
        else item.setText(prot);

        //Year
        item = (TextView) view.findViewById(R.id.year);
        item.setText(String.valueOf(f.getYear()));

        //Country
        item = (TextView) view.findViewById(R.id.country);
        item.setText(f.getCountry());

        //Rate
        /*item = (TextView) view.findViewById(R.id.rate);
        item.setText(String.valueOf(f.getCritics_rate()));*/

        final RatingBar ratBar = (RatingBar) view.findViewById(R.id.ratingBar);
        ratBar.setRating((float)(f.getCritics_rate()/2.0));
        ratBar.setTransitionName(""+films.getPosition(f));

        ratBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, final float v, boolean b) {
                if (b) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    final Film film = films.getItem(Integer.parseInt(ratBar.getTransitionName()));
                    final int ratingValue = film.getCritics_rate();
                    builder.setMessage("The rating of " + film.getTitle() +
                            " is about to change.\n" + "Do you want to continue?");
                    builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            FilmData.updateRatingFilm(film.getId(), (int) (v * 2.0));
                            films.getItem(Integer.parseInt(ratBar.getTransitionName())).setCritics_rate((int) (v * 2.0));
                            RatingBar.OnRatingBarChangeListener listener = ratBar.getOnRatingBarChangeListener();
                            ratBar.setOnRatingBarChangeListener(null);
                            ratBar.setRating(v);
                            ratBar.setOnRatingBarChangeListener(listener);
                            dialogInterface.dismiss();
                            Snackbar snackbar1 = Snackbar.make(view, "Rating updated succesfully", Snackbar.LENGTH_SHORT);
                            snackbar1.setAction("UNDO", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    FilmData.updateRatingFilm(film.getId(), (ratingValue));
                                    films.getItem(Integer.parseInt(ratBar.getTransitionName())).setCritics_rate(ratingValue);
                                    RatingBar.OnRatingBarChangeListener listener = ratBar.getOnRatingBarChangeListener();
                                    ratBar.setOnRatingBarChangeListener(null);
                                    ratBar.setRating((float) (ratingValue/2.0));
                                    ratBar.setOnRatingBarChangeListener(listener);
                                    Snackbar snackbar2 = Snackbar.make(view, "Rating restored!", Snackbar.LENGTH_SHORT);
                                    View viewS = snackbar2.getView();
                                    TextView tv = (TextView) viewS.findViewById(android.support.design.R.id.snackbar_text);
                                    tv.setTextSize(18);
                                    snackbar2.show();
                                }
                            });
                            View viewS = snackbar1.getView();
                            TextView tv = (TextView) viewS.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextSize(18);
                            snackbar1.show();
                        }
                    });
                    builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            RatingBar.OnRatingBarChangeListener listener = ratBar.getOnRatingBarChangeListener();
                            ratBar.setOnRatingBarChangeListener(null);
                            ratBar.setRating((float) (film.getCritics_rate() / 2.0));
                            ratBar.setOnRatingBarChangeListener(listener);
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }
        });

        return view;
    }

    private Spannable highlighText(String text) {
        Spannable spanText = Spannable.Factory.getInstance().newSpannable(text);
        spanText.setSpan(new BackgroundColorSpan(0x80FFFF00), 0, text.length(),Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spanText;
    }
}
