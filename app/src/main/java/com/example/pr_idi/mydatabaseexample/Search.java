package com.example.pr_idi.mydatabaseexample;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by Carlos on 28/12/2016.
 */

public class Search extends Activity {

    private ArrayAdapter<Film> adapter;
    private List<Film> values;
    private ListView list;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search);

        //Get the intent, verify the action and get the query
        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            MainDrawer.filmData.open();
            String query = intent.getStringExtra(SearchManager.QUERY);
            query = intent.getData().toString();
            doSearch(query);
        }
    }

    private void doSearch(String query) {
        Toast.makeText(getApplicationContext(),
                "String received is " + query, Toast.LENGTH_SHORT).show();
        list = (ListView) findViewById(R.id.list_search);
        values = MainDrawer.filmData.getAllFilmsFromProtagonist(query);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, values);
        list.setAdapter(adapter);
    }
}
