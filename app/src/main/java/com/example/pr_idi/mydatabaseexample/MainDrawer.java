package com.example.pr_idi.mydatabaseexample;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import static android.widget.AbsListView.CHOICE_MODE_MULTIPLE;
import static android.widget.AbsListView.CHOICE_MODE_SINGLE;

public class MainDrawer extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static FilmData filmData;
    public static ArrayAdapter<Film> adapter;

    private ExpandableListAdapter expList;
    private ExpandableListView expListView;

    private int previousLength;
    private int sort;

    private SearchView searchView;
    private Menu menuAux;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_drawer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), Alta.class));
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (getSupportActionBar() != null) getSupportActionBar().setTitle("Your films");

        this.sort = 0;
        if (getIntent().hasExtra("isAtoZ")) {
            if (!(getIntent().getExtras().getBoolean("isAtoZ"))) this.sort = 1;
        }

        //Initialize films list
        //final ListView list = (ListView) findViewById(R.id.list);
        expListView = (ExpandableListView) findViewById(R.id.film_list);
        if (getIntent().hasExtra("databaseUpdated")) {
            final Snackbar snackbar = Snackbar.make(expListView, "Database Updated!", Snackbar.LENGTH_LONG);
            View viewS = snackbar.getView();
            TextView tv = (TextView) viewS.findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextSize(18);
            snackbar.show();
        }
        filmData = new FilmData(this);
        filmData.open();

        List<Film> values = filmData.getAllFilms();

        // use the SimpleCursorAdapter to show the
        // elements in a ListView
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, values);
        notifyAdapterDataSetChanged(this.sort == 0);
        //list.setAdapter(adapter);
        expList = new ExpandableListAdapter(this, adapter,getWindowManager(), false);
        expListView.setAdapter(expList);

        expListView./*list.*/setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(final AdapterView<?> adapterView, final View view, final int i, long l) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainDrawer.this);
                builder.setMessage("Do you want to delete this item?");
                builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, final int j) {
                        final Film film;
                        film = (Film) adapter.getItem(i);
                        filmData.deleteFilm(film);
                        adapter.remove(film);
                        expList.setAdapterValues(adapter);
                        expListView.setAdapter(expList);
                        dialogInterface.dismiss();
                        Snackbar snackbar = Snackbar
                                .make(expListView, "Item deleted!", Snackbar.LENGTH_LONG)
                                .setAction("UNDO", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        filmData.createFilm(film.getTitle(), film.getCountry(), film.getYear(), film.getDirector(), film.getProtagonist(), film.getCritics_rate());
                                        adapter.add(film);
                                        if (sort == 0) notifyAdapterDataSetChanged(true);
                                        else notifyAdapterDataSetChanged(false);
                                        expList.setAdapterValues(adapter);
                                        expListView.setAdapter(expList);
                                        Snackbar snackbar1 = Snackbar.make(view, "Item restored!", Snackbar.LENGTH_SHORT);
                                        View viewS = snackbar1.getView();
                                        TextView tv = (TextView) viewS.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextSize(18);
                                        snackbar1.show();
                                    }
                                });
                        View viewS = snackbar.getView();
                        TextView tv = (TextView) viewS.findViewById(android.support.design.R.id.snackbar_text);
                        tv.setTextSize(18);
                        snackbar.show();
                    }
                });
                builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
                return true;
            }
        });

        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                for (int i = 0; i < expList.getGroupCount(); ++i) {
                    if (i != groupPosition) expListView.collapseGroup(i);
                }
            }
        });
    }


    @Override
    protected void onPostResume() {
        super.onPostResume();
        this.sort = 0;
        if (getIntent().hasExtra("isAtoZ")) {
            if (!(getIntent().getExtras().getBoolean("isAtoZ"))) this.sort = 1;
        }
        //ListView list = (ListView) findViewById(R.id.list);
        expListView = (ExpandableListView) findViewById(R.id.film_list);
        List<Film> values = filmData.getAllFilms();
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, values);
        notifyAdapterDataSetChanged((this.sort == 0));
        expList = new ExpandableListAdapter(this, adapter, getWindowManager(), false);
        //list.setAdapter(adapter);
        expListView.setAdapter(expList);
    }

    @Override
    protected void onResume() {
        filmData.open();
        super.onResume();
        if (searchView != null) {
            searchView.onActionViewCollapsed();
            MenuItem menuItem = (MenuItem) menuAux.findItem(R.id.action_sort);
            menuItem.setVisible(true);
        }
    }

    @Override
    protected void onPause() {
        filmData.close();
        super.onPause();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_drawer, menu);

        menuAux = menu;
        //Add SearchWidget
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        MenuItem menuItem = menu.findItem(R.id.options_menu_main_search);
        searchView = (SearchView) MenuItemCompat.getActionView(menuItem);

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        this.previousLength = 0;
        this.sort = 0;

        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MenuItem menuItem = (MenuItem) menu.findItem(R.id.action_sort);
                menuItem.setVisible(false);
                /*menuItem = (MenuItem) menu.findItem(R.id.action_settings);
                menuItem.setVisible(false);*/
                expList.setSearching(true);
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                MenuItem menuItem = (MenuItem) menu.findItem(R.id.action_sort);
                menuItem.setVisible(true);
                /*menuItem = (MenuItem) menu.findItem(R.id.action_settings);
                menuItem.setVisible(true);*/
                expList.setSearching(false);
                collapseAll();
                return false;
            }
        });

        searchView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                return false;
            }
        });

        //searchView.setQueryHint("Search films by protagonist"); //Hint

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String newText) {
                expList.setSearch(newText);
                searchAdapter(newText);
                if (newText.isEmpty()) collapseAll();
                else {
                    for (int i = 0; i < expList.getGroupCount(); ++i) {
                        if (!expListView.isGroupExpanded(i)) {
                            expListView.expandGroup(i);
                        }
                    }
                }
                previousLength = newText.length();
                return true;
                //return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //TODO: Suggestions
                expList.setSearch(newText);
                searchAdapter(newText);
                if (newText.isEmpty()) collapseAll();
                else {
                    for (int i = 0; i < expList.getGroupCount(); ++i) {
                        if (!expListView.isGroupExpanded(i)) {
                            expListView.expandGroup(i);
                        }
                    }
                }
                previousLength = newText.length();
                return true;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private void startSearchActivity(String s) {
        Intent i = new Intent(Intent.ACTION_SEARCH, Uri.parse(s), this, Search.class);
        startActivity(i);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            Toast toast = Toast.makeText(getApplicationContext(), "Settings", Toast.LENGTH_LONG);
            toast.show();
            return true;
        }
        else if (id == R.id.options_menu_main_search) {
            Toast toast = Toast.makeText(getApplicationContext(), "Search", Toast.LENGTH_LONG);
            toast.show();
            return true;
        }
        else */if (id == R.id.sort_atoz) {
            this.sort = 0;
            notifyAdapterDataSetChanged(true);
            expList.setAdapterValues(adapter);
            expListView.setAdapter(expList);
        }
        else if (id == R.id.sort_ztoa) {
            this.sort = 1;
            notifyAdapterDataSetChanged(false);
            expList.setAdapterValues(adapter);
            expListView.setAdapter(expList);
        }
        /*else if (id == R.id.sort_onetotwo) {
            Intent intent = new Intent(this, RecyclerViewClass.class);
            intent.putExtra("isOneToTwo", true);
            startActivity(intent);
        }
        else if (id == R.id.sort_twotoone) {
            Intent intent = new Intent(this, RecyclerViewClass.class);
            intent.putExtra("isOneToTwo", false);
            startActivity(intent);
        }*/

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_alta) {
            startActivity(new Intent(this, Alta.class));
        } else if (id == R.id.nav_recycler) {
            startActivity(new Intent(this, RecyclerViewClass.class));
        } else if (id == R.id.nav_about) {
            startActivity(new Intent(this, AboutActivity.class));
        } else if (id == R.id.nav_help) {
            startActivity(new Intent(this, HelpActivity.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    // Will be called via the onClick attribute
    // of the buttons in activity_main_drawer.xml
    public void onClick(View view) {
        @SuppressWarnings("unchecked")
        //ArrayAdapter<Film> adapterList = (ArrayAdapter<Film>) getListAdapter();
        Film film;
        switch (view.getId()) {
            /*case R.id.add:
                String[] newFilm = new String[] { "Blade Runner", "Ridley Scott", "Rocky Horror Picture Show", "Jim Sharman", "The Godfather", "Francis Ford Coppola", "Toy Story", "John Lasseter" };
                int nextInt = new Random().nextInt(4);
                // save the new film to the database
                film = filmData.createFilm(newFilm[nextInt*2], "Catalonia", 2014, newFilm[nextInt*2 + 1], "Do not know", 5);
                adapter.add(film);
                break;
            case R.id.delete:
                if (adapter.getCount() > 0) {
                    film = (Film) adapter.getItem(0);
                    filmData.deleteFilm(film);
                    adapter.remove(film);
                }
                break;*/
            /*case R.id.altaButton:
                startActivity(new Intent(this, Alta.class));
                break;
            case R.id.recyclerViewButton:
                startActivity(new Intent (this, RecyclerViewClass.class));
                break;*/
        }
        notifyAdapterDataSetChanged(this.sort == 0);
    }

    private void notifyAdapterDataSetChanged(final boolean ascending) {
        adapter.sort(new Comparator<Film>() {
            @Override
            public int compare(Film f1, Film f2) {
                if (ascending) return f1.getTitle().compareToIgnoreCase(f2.getTitle());
                return (f1.getTitle().compareToIgnoreCase(f2.getTitle()) * -1);
            }
        });
        adapter.notifyDataSetChanged();
    }

    private void searchAdapter(String protagonist) {
        if (protagonist.length() < previousLength) {
            List<Film> values = filmData.getAllFilms();
            adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, values);
        }
        if (protagonist.length() > 0) {
            Film f;
            String lower, prot, prot_low, title, title_low;
            for (int i = 0; i < adapter.getCount(); ++i) {
                f = adapter.getItem(i);
                //if (!f.getProtagonist().contains(protagonist.subSequence(0, protagonist.length()))) {
                prot = f.getProtagonist();
                prot_low = prot.toLowerCase();
                lower = protagonist.toLowerCase();
                title = f.getTitle();
                title_low = title.toLowerCase();
                //Toast.makeText(getApplicationContext(), "Lower: " + lower + "\nUpper: " + upper, Toast.LENGTH_SHORT).show();
                //if (!(f.getProtagonist().matches(protagonist.concat("(.*)")))) {
                if ((!prot.matches(protagonist.concat("(.*)"))) && (!prot_low.matches(lower.concat("(.*)"))) &&
                    (!title.matches(protagonist.concat("(.*)"))) && (!title_low.matches(lower.concat("(.*)")))) {
                    adapter.remove(f);
                    --i;
                }
            }
        }

        notifyAdapterDataSetChanged(this.sort == 0);
        //ListView list = (ListView) findViewById(R.id.list);
        //list.setAdapter(adapter);
        expListView = (ExpandableListView) findViewById(R.id.film_list);
        expList.setAdapterValues(adapter);
        expListView.setAdapter(expList);
    }

    private void collapseAll() {
        for (int i = 0; i < expList.getGroupCount(); ++i) {
            if (expListView.isGroupExpanded(i)) {
                expListView.collapseGroup(i);
            }
        }
    }

}
