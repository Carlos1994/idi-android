package com.example.pr_idi.mydatabaseexample;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.webkit.WebView;
import android.widget.TextView;

/**
 * Created by eric on 5/01/17.
 */

public class HelpActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        /*TextView help = (TextView) findViewById(R.id.textViewHelp);
        help.setText (Html.fromHtml (getString (R.string.help_string)));
        help.setMovementMethod(new ScrollingMovementMethod());*/

        WebView help = (WebView) findViewById(R.id.textViewHelp);
        help.loadData(getString(R.string.help_string), "text/html; charset=utf-8", "utf-8");
        help.setBackgroundColor(Color.TRANSPARENT);
        help.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
    }
}
