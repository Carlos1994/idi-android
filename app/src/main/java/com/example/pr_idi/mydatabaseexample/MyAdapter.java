package com.example.pr_idi.mydatabaseexample;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by eric on 25/12/16.
 */

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private List<Film> mDataset;

    private Activity context;
    private Boolean order;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView textView;
        public TextView directorView;
        public TextView protagonistView;
        public TextView yearView;
        public TextView countryView;
        public RatingBar ratingView;

        public View view;

        public ViewHolder(View v){

            super(v);

            textView = (TextView)v.findViewById(R.id.subject_textview);
            directorView = (TextView)v.findViewById(R.id.director);
            protagonistView = (TextView)v.findViewById(R.id.protagonist);
            yearView = (TextView)v.findViewById(R.id.year);
            countryView = (TextView)v.findViewById(R.id.country);
            ratingView = (RatingBar)v.findViewById(R.id.ratingBar);
            this.view = v;
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MyAdapter(List<Film> myDataset, Boolean isOneToTwo, Activity ctx) {
        order = isOneToTwo;
        mDataset = myDataset;
        this.context = ctx;
        if (mDataset.size() > 0) {
            if (isOneToTwo) {
                Collections.sort(mDataset, new Comparator<Film>() {
                    @Override
                    public int compare(final Film object1, final Film object2) {
                        if (object1.getYear() < object2.getYear()) return -1;
                        else if (object1.getYear() > object2.getYear()) return 1;
                        return 0;
                    }
                });
            } else {
                Collections.sort(mDataset, new Comparator<Film>() {
                    @Override
                    public int compare(final Film object1, final Film object2) {
                        if (object1.getYear() > object2.getYear()) return -1;
                        else if (object1.getYear() < object2.getYear()) return 1;
                        return 0;
                    }
                });
            }
        }
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_items, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.textView.setText(mDataset.get(position).getTitle());
        holder.directorView.setText(mDataset.get(position).getDirector());
        holder.protagonistView.setText(mDataset.get(position).getProtagonist());
        holder.yearView.setText(Integer.toString(mDataset.get(position).getYear()));
        holder.countryView.setText(mDataset.get(position).getCountry());
        holder.ratingView.setRating((float) (mDataset.get(position).getCritics_rate()/2.0));
        holder.ratingView.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, final float v, boolean b) {
                if (b) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    final Film film = mDataset.get(position);
                    final int ratingValue = film.getCritics_rate();
                    builder.setMessage("The rating of " + film.getTitle() +
                            " is about to change.\n" + "Do you want to continue?");
                    builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            FilmData.updateRatingFilm(film.getId(), (int) (v * 2.0));
                            film.setCritics_rate((int) (v * 2.0));
                            mDataset.set(position,film);
                            RatingBar.OnRatingBarChangeListener listener = holder.ratingView.getOnRatingBarChangeListener();
                            holder.ratingView.setOnRatingBarChangeListener(null);
                            holder.ratingView.setRating(v);
                            holder.ratingView.setOnRatingBarChangeListener(listener);
                            dialogInterface.dismiss();
                            Snackbar snackbar1 = Snackbar.make(holder.view, "Rating updated succesfully", Snackbar.LENGTH_SHORT);
                            snackbar1.setAction("UNDO", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    FilmData.updateRatingFilm(film.getId(), (ratingValue));
                                    film.setCritics_rate(ratingValue);
                                    mDataset.set(position,film);
                                    RatingBar.OnRatingBarChangeListener listener = holder.ratingView.getOnRatingBarChangeListener();
                                    holder.ratingView.setOnRatingBarChangeListener(null);
                                    holder.ratingView.setRating((float) (ratingValue/2.0));
                                    holder.ratingView.setOnRatingBarChangeListener(listener);
                                    Snackbar snackbar2 = Snackbar.make(view, "Rating restored!", Snackbar.LENGTH_SHORT);
                                    View viewS = snackbar2.getView();
                                    TextView tv = (TextView) viewS.findViewById(android.support.design.R.id.snackbar_text);
                                    tv.setTextSize(18);
                                    snackbar2.show();
                                }
                            });
                            View viewS = snackbar1.getView();
                            TextView tv = (TextView) viewS.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextSize(18);
                            snackbar1.show();
                        }
                    });
                    builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            RatingBar.OnRatingBarChangeListener listener = holder.ratingView.getOnRatingBarChangeListener();
                            holder.ratingView.setOnRatingBarChangeListener(null);
                            holder.ratingView.setRating((float) (film.getCritics_rate() / 2.0));
                            holder.ratingView.setOnRatingBarChangeListener(listener);
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }
        });

        holder.view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(final View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("Do you want to delete this item?");
                builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, final int i) {
                        final Film film;
                        film = (Film) mDataset.get(position);
                        MainDrawer.filmData.deleteFilm(film);
                        mDataset.remove(position);
                        RecyclerViewClass.mAdapter = new MyAdapter(mDataset,order,context);
                        RecyclerViewClass.mRecyclerView.setAdapter(RecyclerViewClass.mAdapter);
                        dialogInterface.dismiss();
                        Snackbar snackbar = Snackbar
                                .make(RecyclerViewClass.mRecyclerView, "Item deleted!", Snackbar.LENGTH_LONG)
                                .setAction("UNDO", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        MainDrawer.filmData.createFilm(film.getTitle(), film.getCountry(), film.getYear(), film.getDirector(), film.getProtagonist(), film.getCritics_rate());
                                        mDataset.add(film);
                                        RecyclerViewClass.mAdapter = new MyAdapter(mDataset,order,context);
                                        RecyclerViewClass.mRecyclerView.setAdapter(RecyclerViewClass.mAdapter);
                                        Snackbar snackbar1 = Snackbar.make(view, "Item restored!", Snackbar.LENGTH_SHORT);
                                        View viewS = snackbar1.getView();
                                        TextView tv = (TextView) viewS.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextSize(18);
                                        snackbar1.show();
                                    }
                                });
                        View viewS = snackbar.getView();
                        TextView tv = (TextView) viewS.findViewById(android.support.design.R.id.snackbar_text);
                        tv.setTextSize(18);
                        snackbar.show();
                    }
                });
                builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
                return true;
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}


